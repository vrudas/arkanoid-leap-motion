﻿using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private Paddle _paddle;
    private Rigidbody _rigidbody;
    public List<GameObject> Blocks;
    public float CurrentSpeed;
    public float MaxSpeed;
    public float MinSpeed;
    public float SpeedMultiplier;
    // Use this for initialization
    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _paddle = FindObjectOfType<Paddle>();
        Blocks = FindBlocks();

        ResetBall();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Blocks.Count == 0)
        {
            GameWon();
        }
    }

    private void FixedUpdate()
    {
        CurrentSpeed = Vector3.Magnitude(_rigidbody.velocity);

        if (CurrentSpeed > MaxSpeed)
        {
            _rigidbody.velocity /= CurrentSpeed/MaxSpeed;
        }

        if (CurrentSpeed < MinSpeed && CurrentSpeed > 0)
        {
            _rigidbody.velocity /= CurrentSpeed/MinSpeed;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Hit: " + collision.gameObject.name);
        _rigidbody.velocity += _rigidbody.velocity*SpeedMultiplier;

        if (collision.gameObject.CompareTag("Block"))
        {
            Blocks.Remove(collision.gameObject);
        }

        Debug.LogWarning("Blocks left: " + Blocks.Count);
    }

    private void OnBecameInvisible()
    {
        Debug.Log("Out of Camera");

        if (Lives.CurrentLives-- < 2)
        {
            GameOver();
        }
        else
        {
            ResetBall();
        }
    }

    private void GameOver()
    {
        Debug.Log("Game Over!");
        GetComponent<Renderer>().enabled = false;
        _rigidbody.velocity = Vector3.zero;
        transform.position = Vector3.zero;
    }

    private void ResetBall()
    {
        _rigidbody.position = new Vector3(_paddle.transform.position.x + _paddle.transform.localScale.y,
            Paddle.yPosition + _paddle.transform.localScale.y, Paddle.zPosition);
        transform.LookAt(_paddle.transform.position);

        _rigidbody.AddRelativeForce(new Vector3(0, 0, MinSpeed));
    }

    private List<GameObject> FindBlocks()
    {
        var blocks = new List<GameObject>();
        blocks.AddRange(GameObject.FindGameObjectsWithTag("Block"));

        return blocks;
    }

    private void GameWon()
    {
        Debug.Log("Game Won");
    }
}