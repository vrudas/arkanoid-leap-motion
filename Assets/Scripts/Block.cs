﻿using Assets.Scripts;
using UnityEngine;

public class Block : MonoBehaviour
{
    public BlockType BlockType = BlockType.BlockRed;
    public int Points = 10;
    // Use this for initialization
    private void Start()
    {
        var meshRenderer = GetComponent<MeshRenderer>();
        var blockMaterial = Resources.Load(BlockType.ToString()) as Material;
        meshRenderer.material = blockMaterial;
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {
        Score.score += (int) BlockType;
        Destroy(gameObject);
    }
}