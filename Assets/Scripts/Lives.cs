﻿using UnityEngine;
using UnityEngine.UI;

public class Lives : MonoBehaviour
{
    private const string LivesText = "Lives: ";
    public static int CurrentLives;
    private Text _text;
    public int StartingLives = 3;
    // Use this for initialization
    private void Start()
    {
        CurrentLives = StartingLives;
        _text = GetComponent<Text>();
    }

    // Update is called once per frame
    private void Update()
    {
        _text.text = LivesText + CurrentLives;
    }
}