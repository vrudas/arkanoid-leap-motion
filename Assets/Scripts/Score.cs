﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public static int score = 0;
    private Text _text;
    // Use this for initialization
    private void Start()
    {
        _text = GetComponent<Text>();
    }

    // Update is called once per frame
    private void Update()
    {
        _text.text = score.ToString();
    }
}