﻿using Leap;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    public static float zPosition = 0;
    public static float yPosition = -0.9f;
    private Controller controller;
    public float speed = 5;
    public float wallScale = 0;
    public static float xBoundary = 9.3f;
    // Use this for initialization
    private void Start()
    {
        controller = new Controller();
        transform.position = new Vector3(0, yPosition, zPosition);
    }

    // Update is called once per frame
    private void Update()
    {
        if (controller.IsConnected)
        {
            MovePaddleLeap();
        }
        else
        {
            MovePaddleKeyboard();
        }
    }

    private void MovePaddleLeap()
    {
        var frame = controller.Frame();

        var wantedPos = new Vector3(frame.Hands[0].StabilizedPalmPosition.x, frame.Hands[0].PalmPosition.y, 0);
        wantedPos.x *= 0.1f;
        wantedPos.y = yPosition;
        wantedPos.z = zPosition;

//        Debug.Log(wantedPos);
        transform.position = wantedPos;

        CheckBounds();
    }

    private void MovePaddleKeyboard()
    {
        var input = Input.GetAxis("Horizontal");
        if (input != 0)
        {
            transform.position = new Vector3(transform.position.x + input*speed*Time.deltaTime, yPosition, zPosition);

            CheckBounds();
        }
    }

    private void CheckBounds()
    {
        var correctedX = transform.localScale.x/2 + wallScale/2;
        if (transform.position.x < -xBoundary + correctedX)
        {
            transform.position = new Vector3(-xBoundary + correctedX, yPosition, zPosition);
        }
        else if (transform.position.x > xBoundary - correctedX)
        {
            transform.position = new Vector3(xBoundary - correctedX, yPosition, zPosition);
        }
    }
}